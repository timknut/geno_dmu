"""
Starts out with one genotye file (affy format) containing the genotypes of a subset of the total amount of SNPs, and with a phenotype file. 
These files must contain the same animals in the same order. 
For each SNP, makes a data file and two dir files (H0 and H1). Then runs DMU under H0 and under H1. Finally, reads the output and puts the results into a summary file. 

"""

import sys
import dmu
import os

from scipy.stats import chi2

currthread, genofilename, phenofilename, tempdir, no_of_effects_H1, outfolder, templatefileH0name, templatefileH1name, missingindicator = tuple(sys.argv[1:10])	#different info passed from DMU_super.py

phenofile = file(phenofilename,'r')	#a file containing all the columns needed for the analysis, except the gneotypes
genofile = file(genofilename,'r')	#this is a file in affy-format, but a tranposed one, i.e. the markers appear in rows. 
templatefile_H0DIR = file(templatefileH0name,'r')
templatefile_H1DIR = file(templatefileH1name,'r')
outfile = file(outfolder + currthread + '.out', 'w')

#print header of outputfile: 
outfile.write('SNP\tH0-2logL')
for k in range(int(no_of_effects_H1)-1):
	outfile.write('\t' + 'H0_varcomp' + str(k))
outfile.write('\tH1-2logL')
for k in range(int(no_of_effects_H1)):
	outfile.write('\t' + 'H1_varcomp' + str(k))
outfile.write('\tchi2\tp-value\tfraction_of_variance_explained\tn_animals_with_data\tfreq\tdatafile\n')

#read phenofile: 
dphenos = {}
for line in phenofile: 
	llin = line.strip().split('\t')
	dphenos[llin[0]] = ' '.join(llin) #d = {<animal1ID>: <entire line.strip() in phenofile>, .....}

#loop through all markers: 
lanimals = genofile.readline().strip().split('\t')	#may be needed later on, or perhaps not
ncurrmarker = 0
for line in genofile: 	#done for each marker
	
	#make data file: 
	llin = line.strip().split('\t')	#read genotypes of that marker
	SNP = llin[0]
	ncurrmarker += 1	#may be needed 
	datafilename = tempdir + currthread + '_' + str(ncurrmarker) + '.data'
	datafile = file(datafilename, 'w')
	nanimals = 0	#no of animals with genotype
	allele2count, totalcount = 0, 0 #[sum of numbers of allele 1, total number of allele copies]	

	
	currgenos = []	#new des13
	for k in range(1, len(lanimals)):
		currgeno = llin[k]
		if currgeno in ['0','1','2']: currgenos.append(currgeno)	#new des13
		if not currgeno == missingindicator:
			datafile.write('1 ' + dphenos[lanimals[k]] + ' ' + currgeno + '\n')	#only writes line if the genotype is not missing. 
			nanimals += 1
			allele2count += int(currgeno)
			totalcount += 2

	allelefreq = (allele2count+0.0)/totalcount
	datafile.close()

	#added des13: 
	if len(set(currgenos)) <= 1: 
		os.remove(datafilename)
		continue
	#end of added		

	#make DIR-H0-file: 
	DIRfilename = tempdir + currthread + '_' + str(ncurrmarker) + '.H0.dir'
	dirfile = file(DIRfilename, 'w')
	templatefile_H0DIR.seek(0)
	for line in templatefile_H0DIR: 
		dirfile.write(line.replace('DATAFILENAME', datafilename))
	dirfile.close()

	#run under H0
	RESfilename = tempdir + currthread + '_' + str(ncurrmarker) + '.H0.res'
	os.system('dmu1 <' + DIRfilename + ' >' + RESfilename)
	os.system('dmuai <' + DIRfilename + ' >>' + RESfilename)
	
	#parse H0-results adn remove files: 
	resH0, err, crash_err = dmu.parse_DMU_output(RESfilename,int(no_of_effects_H1)-1)
	os.remove(DIRfilename)
	#if err == '' and crash_err == '': os.remove(RESfilename)
	os.remove(RESfilename)


	#make DIR-H1-file:
	DIRfilename = tempdir + currthread + '_' + str(ncurrmarker) + '.H1.dir'
	dirfile = file(DIRfilename, 'w')
	templatefile_H1DIR.seek(0)
	for line in templatefile_H1DIR: 
		dirfile.write(line.replace('DATAFILENAME', datafilename))
	dirfile.close()	

	#run under H1
	RESfilename = tempdir + currthread + '_' + str(ncurrmarker) + '.H1.res'
	os.system('dmu1 <' + DIRfilename + ' >' + RESfilename)
	os.system('dmuai <' + DIRfilename + ' >>' + RESfilename)

	#parse H1-results and remove files: 
	resH1, err, crash_err = dmu.parse_DMU_output(RESfilename,int(no_of_effects_H1))
	os.remove(DIRfilename)
	#if err == '' and crash_err == '': os.remove(RESfilename)
	os.remove(RESfilename)


	#calculate:
	if crash_err == '':
		varcomp = (resH1[2]+0.0)/sum(resH1[1:])
		chisq = resH0[0]-resH1[0]
		pvalue = 1-chi2.cdf(chisq,1)
		outfile.write(SNP + '\t' + '\t'.join([str(a) for a in resH0]) + '\t' + '\t'.join([str(a) for a in resH1]) + '\t' + str(chisq) + '\t' + str(pvalue) + '\t' + str(varcomp) + '\t' + str(nanimals) + '\t' + str(allelefreq) + '\t' + datafilename + '\n')

	else:
		outfile.write(SNP + '\t' + crash_err + '\n')	
	
	#remove files that are no longer needed:
	os.remove(datafilename)	
	

outfile.close()
genofile.close()
phenofile.close()
templatefile_H0DIR.close()
templatefile_H1DIR.close()
