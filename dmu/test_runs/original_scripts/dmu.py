
def parse_DMU_output(resfilename, noofvarcomps):
	
	#returns res, err, crash_err.
	#the two latter are strings, the first is [<-2LogL>,<varcomp1>, <varcomp2>, ..., <varcomp3>] 

	resfile = file(resfilename,'r')
	res = ['']*(noofvarcomps+1)
	err = ''
	crash_err = ''

	#print resfile
	l = 0
	
	#terminated = False
	while 1:
		lin = resfile.readline().strip()
		if 'DMUAI terminates' in lin: 
			err = 'DMAUI terminates'
			return	([1.0]*(noofvarcomps+1), err, crash_err)

		if '-2LogL ..............' in lin: break
		


	res[0] = float(lin.replace('-2LogL ..........................................','').replace('+ constant','').strip())
	while resfile.readline().strip() != 'Asymptotic SE based on AI-information matrix':
		pass
		

	for k in range(3):
    		resfile.readline().strip()
	
	for k in range(noofvarcomps):
		lin = resfile.readline().strip()
		llin = lin.split()
		try:
			res[k+1] = float(llin[-2])
		except IndexError:
			crash_err = 'too few VCs estimated' 
			return (res, err, crash_err)

		if len(llin) != 3:
			err += 'varcomp ' + str(k) + ': ' + lin
		
	resfile.close()
	return (res, err, crash_err)
