
def split_between(whole, part1, part2):
    a = whole.split(part1)
    b = []
    for i in range(1,len(a)):
        b.append(a[i].split(part2)[0])
    return b
        
def insert_0_in_numbers(number, digits):
    number = '0'*(digits-len(number)) + str(number)
    return number

def replace_multiplespaces_with_singlespaces(instring):
    s = instring.split()
    out = ''
    for i in range(len(s)):
        out = out + s[i] + ' '
    out = out[:len(out)-1]
    return out

def remove_numbers_from_string(instring):
    outstring = ''    
    for i, c in enumerate(instring):
        if not (48<= ord(c) <= 57): 
            outstring = outstring + c
    return outstring

def string_from_list(lst, delimiter):
    outstr = lst[0]
    for i in range(1,len(lst)):
        outstr = outstr + delimiter + lst[i]
    return outstr


def sum_vector(v, add):
    m = min([len(v), len(add)])
    for k in range(m):
        v[k] += add[k]
    return v

def reciproc(str1,str2,str3):
    out = ''
    if str1 == str2:
        out = str3
    elif str1 ==str3:
        out = str2
    else:
        out = None
    return out

def otherone(str1,str2,str3):
    out = ''
    if str1 == str2:
        out = str3
    elif str1 ==str3:
        out = str2
    else:
        out = None
    return out

def sortallelesingeno(geno, delim):
    lgeno = geno.split(delim)
    if len(lgeno) != 2:
        print 'error: not 2 alleles (util.sortallelesingeno)'
        print lgeno
    else:
        if lgeno[0] > lgeno[1]:
            geno = lgeno[1] + delim + lgeno[0]
    return geno

    
def isitthesamegene(str1, str2):

    lst1 = str1.strip().split()
    lst2 = str2.strip().split()

    overlap = set(lst1).intersection(set(lst2))
    nonoverlap = set(a).union(set(b)) - overlap

    if overlap >= nonoverlap:
        return True
    else:
        return False
    
def column(matrix, i):
    """returns column i in two-dimensional array"""
    return [row[i] for row in matrix] 

def transpose_file_newest(infilename, outfilename):

	#note that there must be the same number of column in each line of infile

	infile = file(infilename, 'r')
	outfile = file(outfilename, 'w')

	L = []
	for line in infile: 
		L.append(line.strip().split('\t'))
	
	L_t = zip(*L)
	for rw in L_t:
		outfile.write('\t'.join(rw) + '\n')

	infile.close()
	outfile.close()
	
	
	
	
	
	
	
	
