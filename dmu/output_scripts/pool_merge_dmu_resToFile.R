#!/local/genome/packages/R/3.0.2shlib/bin/env Rscript --slave  
# this way the script will not have to be in path to run with Rscript script.R

# Pool and subset output, make manhattan plot and write missing markers to file. 

# This script is inteded for use with *.out files from dmu-GWAS and 777k_markerinfo.txt from same data-set.
# It will merge all output-files, extract significant SNPs and write all results and sign results to separate files.
# This script will also identify markers in 777k_markerinfo NOT run by DMU, and write these markers to file.
# This script will ignore incomplete lines in out files resulting from dmu errors.
# This script will also write a manhattan and qq plot to png files.

####### Set correct trait and paths ##############
 trait <- "slaktevekt"
 path <- sprintf("~/dmu/results/geno-runs-affy/%s/out/", trait)   # path to out folder
 resfolder <- sprintf("~/dmu/results/geno-runs-affy/results/%s/", trait) # check path for res files. Will be created by script. 
############# DO NOT EDIT BELOW THIS LINE! ##############################
 
 require(parallel)  # package for parallel computing. 
 n.cores = 10

# Setting file and folder names -------------------------------------------
 allres.out <- sprintf("%s_all_results.txt", trait)              # filename for output.
 signfile.out <- sprintf("%s_sign_results.txt", trait)           # filename for sign output.
 outfile.missing <- sprintf("%s/missing_%s", resfolder, trait)   # outfile for missing SNPs (not run by DMU)
 markerinfo.file <- "~/dmu/DMU-data/777k_markerinfo.txt"

 # Set Working Directory -------------------------------------------------
 setwd(path) 
 cat ("Setting Working Direcory to:", getwd(), "\n")
 # stopifnot(grepl("*/out", getwd())) # program fails if not in '*/out/'

 # read out files and edit names ----------------------------------------------------------
 cat ("reading *.out files..")
 filenames <- list.files(path = path, full.names=TRUE, pattern = "*.out") # read *.out file names
 headers <- read.delim(filenames[1], nrows=1, header=FALSE,stringsAsFactors=FALSE) # read header
 time.spent <- system.time (all.files <- do.call("rbind", mclapply(filenames, read.delim, 
                                                 header = FALSE, skip=1, mc.cores = n.cores))) # skip=1 to skip header
 cat(" Done\n", "time spent: ", time.spent[3], " seconds\n", sep="")
 names(all.files) <- headers                         # add header to concatenated file.
 names(all.files)[which(grepl("p-value", (names(all.files))))] <- "P" # search and replace "p-value" with "P".

# check for uncomplete rows and remove -----------------------------------------------
 complete <- complete.cases(all.files) 
 all.files <- all.files[complete,]

# Make significant subset ----------------------------------------------------------------
 significant.SNPs <- subset(all.files, P <= 0.01 & fraction_of_variance_explained >= 0.001) # create sign SNPs subset.
 significant.SNPs.sort_on_P <- significant.SNPs[order(significant.SNPs$P),] # sort on P.


 # create resfolder and set WD to it------------------------------------------------------
 dir.create(file.path(resfolder), showWarnings = FALSE) # doesn't care if folder exists.
 setwd(file.path(resfolder))

 # Missing markers are identified and written to file ------------------
 cat ("Calculating missing markers..\n")
# SNPs.1.run <- read.table(pipe(sprintf("cut -f1 %s", outfile.all.files )), stringsAsFactors=F, 
 #                         comment.char="", header=TRUE ) # pipes unix command cut for faster performance
 SNPs.1.run <- all.files[1]
 All.markers <- read.table(pipe(sprintf("cut -f1 %s", markerinfo.file)), header=F, 
                           stringsAsFactors=F, comment.char="", col.names="SNP") # OBS!! filepath
 
 pos.duplicates <- match(unlist(SNPs.1.run), unlist(All.markers)) #  need to unlist data.frame for match to work.
 for.retesting <- All.markers[-pos.duplicates,]
 write(for.retesting, file = outfile.missing)
 
 # for merging on column "name" --------------------------------------------------------
 names(all.files)[1]  <- "name" 
 names(significant.SNPs.sort_on_P)[1] <- "name"
 
 # Read file with markerinfo ----------------------------------------------- 
 classes <- c("character","integer","character","integer",rep("character",3)) # for faster read.table performance
 markers <- read.table(markerinfo.file, col.names=c("name","nr","SNP","BP","A1","A2","CHR"), 
                       comment.char="", colClasses=classes)
 markers <- subset(markers, select =-nr) # delete the "nr" column.
 
 # Merge objects -------------------------------------------------------------
 all.merged <- merge(all.files, markers)
 sign.merged <- merge(significant.SNPs.sort_on_P, markers)
 
 # Subset objects and sort on P for sign ---------------------------------------------------------
 manhattan.subset.all <- subset(all.merged, select = c(SNP, CHR, BP, P, chi2, 
                                                       fraction_of_variance_explained, A1, A2, freq))
 # manhattan.subset.all <- manhattan.subset.all[order(manhattan.subset.all$CHR),]
 manhattan.subset.sign <- subset(sign.merged, select = c(SNP, CHR, BP, P, chi2, 
                                                         fraction_of_variance_explained, A1, A2, freq))
 manhattan.subset.sign <- with (manhattan.subset.sign, manhattan.subset.sign[order(P),]) # sort on p-value
 
 rm(all.merged, all.files, markers) # clear memory 

 # manhattan and qq plot -----------------------------------------------------------------
  cat("Making Manhattan plot..\n")
 
 source("/mnt/users/tikn/Projects/qqman/qqman.r") # source the script containing the functions.
 class(manhattan.subset.all$CHR) <- "integer" # plot will fail if class($CHR) == "charcter"
 png(sprintf("manhattan_%s.png", trait), width = 1400, height=800) # open plotting device
 manhattan(subset(manhattan.subset.all, fraction_of_variance_explained >= 0.00001), pt.col=c("black","grey50","darkorange1"),
           pch=20, main = sprintf("DMU %s", trait))
 dev.off() 

 # qq plot -----------------------------------------------------------------
 qq_subset <- subset(manhattan.subset.all, fraction_of_variance_explained >= 0.0000001, select=P)
 png(sprintf("qq-plot_dmu_%s.png", trait), width = 1200, height=600) # open plotting device
 qq(qq_subset$P, main=sprintf("qq plot %s", trait))
 dev.off()

# ## For use where infile exists
# png(sprintf("manhattan_%s.png", trait), width = 1400, height=800) # open plotting device 
# manhattan(temp, pt.col=c("black","grey50","darkorange1"), pch=20,
#            suggestiveline=F, main = sprintf("Manhattan plot %s", trait))
#  dev.off() 
   
 # Write results to files ----------------------------------------------------------
 cat("Writing results to files\n")
 write.table(manhattan.subset.all, file = allres.out, quote=FALSE, row.names=FALSE, sep="\t")
 write.table(manhattan.subset.sign, file = signfile.out, quote=FALSE, row.names=FALSE, sep="\t")


 cat("Done\n", "Results are in: ", resfolder, "\n", sep="","You have ", length(list.files(recursive=TRUE)),
     " files in: ", resfolder, " and might need to clean up..\n")


## make vcf and run snpeff.
#### husk #####
# lage path til configfil og databsen som egne variabler med full path slik at programmet blir generisk.
##############################################

 vcf <- manhattan.subset.sign[c(2,3,1,7,8)]   # sorting values
 names(vcf) <- c("#CHROM", "POS", "ID", "REF", "ALT") # propper naming

 vcf <- within(vcf,  {           # adding relevant columns
   INFO <- "."
   FILTER <- "."
   QUAL <- "."
 })

 snpeff <- "/local/genome/packages/snpeff/3.4/snpEff.jar"
 config <- "/mnt/users/tikn/snpeff/snpEff.config"
 write.table(vcf, sprintf("%s.vcf", trait), quote=FALSE, sep="\t", row.names=FALSE)
 system(sprintf("java -Xmx4g -jar %s eff -c %s -stats snpEff_summary_%s.html -v UMD3.1.74 %s.vcf > %s.eff.vcf", 
                snpeff ,config, trait, trait, trait))

 rm(list=ls()) # CATION! remove old variables from memory
